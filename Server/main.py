from flask import Flask, request, render_template, render_template_string
import logging
import json
import re
import requests

app = Flask(__name__)
app.logger.setLevel(logging.INFO)
app.debug = True
app.secret_key = '<FLAG2=1E2t7&>'

def check_uptime(URL):
    try:
        r = requests.get(URL, verify=False, timeout=0.5)

        if (r.status_code == 200):
            return True
        else:
            return False
    except requests.exceptions.RequestException as e:
        return False


@app.get('/')
def get_easy_login():
    f = open("lab_machines.txt", "r")
    lab_machines = {}
    lab_obj = []
    for line in f:
        line = line.rstrip()
        line = line.split()
        lab_machines[line[0]] = check_uptime(line[1])
        lab_machine = {}
        lab_machine['link'] = line[1]
        lab_machine['name'] = line[0]
        lab_machine['status'] = check_uptime(line[1])
        lab_obj.append(lab_machine)

    f.close()
    f = open("ctf_machines.txt", "r")
    ctf_machines = {}
    ctf_obj = []
    for line in f:
        line = line.rstrip()
        line = line.split()
        ctf_machines[line[0]] = check_uptime(line[1])
        ctf_machine = {}
        ctf_machine['link'] = line[1]
        ctf_machine['name'] = line[0]
        ctf_machine['status'] = check_uptime(line[1])
        ctf_obj.append(ctf_machine)
    f.close()
    

    return render_template('index.html', lab_machines=lab_obj, ctf_machines=ctf_obj),200

@app.post('/add')
def post_easy_login():
    try:
        # Check media type
        if not request.is_json:
            return {'error': 'Request must be JSON'}, 415

        # Retrieve body
        body = request.get_json()
        if body is None:
            return {'error': 'Missing body'}, 400

        # Validate body schema
        keys = body.keys()
        if not len(keys) == 1 or 'items' not in keys:
            return {'error': 'Body must contain only \'item\' fields'}, 400

        # Extract values
        obj = body['items']

        template = '<li>' + obj + '</li>'

        if obj is None:
            return {'error': 'Invalid item'}, 403

        msg = render_template_string(template)
        flag = ''

        if cleanhtml(msg) == '56': #7*8
            flag = "FLAG1=uijfaw"
            return {'message': msg, 'flag': flag}, 200

        # Return success message
        return {'message': msg},200
    except Exception as e:
        app.logger.warn(e)
        return f"{e}", 500

CLEANR = re.compile('<.*?>') 

def cleanhtml(raw_html):
  cleantext = re.sub(CLEANR, '', raw_html)
  return cleantext
