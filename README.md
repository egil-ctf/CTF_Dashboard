# CTF Dashboard Overview

# Table of Contents
1. [Introduction](#introduction)
2. [Starting and Stopping the Project](#starting-and-stopping-the-project)
3. [Course Content: LAB and CTF Challenges](#course-content-lab-and-ctf-challenges)
4. [Future Plans](#future-plans)
5. [Technology Used](#technology-used)


## Introduction
The Machine Dashboard serves as a centralized portal for users to navigate through various hosted labs and assignments, including Capture The Flag (CTF) challenges. It also provides functionality to monitor the uptime of these services. As part of future developments, there are plans to enhance its capabilities, especially in the context of teaching penetration testing.

## Starting and Stopping the Project

### Starting the Project
To start the project, use the following Docker Compose command. This will build and launch all the necessary containers in detached mode, allowing them to run in the background.

```docker compose up -d --build```

This command performs two main actions:

1. --build: Builds the images for the containers if they don't already exist or need updating.
2. -d: Runs the containers in detached mode, freeing up the terminal and running the containers in the background.

### Stopping the Project
When you're finished and want to stop all the running containers associated with the project, use the following command:

```docker compose down```

This command safely stops and removes all the containers, networks, and volumes created by docker compose up, cleaning up the system.

## Course Content: LAB and CTF Challenges

### LAB Challenges
- **Focus**: Concentrating on single security vulnerabilities.
- **Duration**: Designed to be completed in about one hour.
- **Objective**: Offer focused learning, ideally paired with lectures for practical application.
- **Key Features**: Targeted exploration of specific vulnerabilities; concise and suitable for classroom integration.

### CTF Challenges
- **Scope**: Cover a broad range of security vulnerabilities.
- **Duration**: Completion may take several days or intensive hours.
- **Objective**: Provide a comprehensive and realistic cybersecurity experience.
- **Application**: Used as assignments to test cumulative knowledge from LABs and lectures.
- **Key Features**: Require diverse skill application; foster deep engagement and understanding of complex issues.

## Future Plans

### 1. User-Specific Instances
- **Objective**: Develop a secure portal enabling users to independently start and stop their instances.
- **Implementation**: Integration of a mechanism for users to establish a secure connection to their instances via a Wireguard gateway.

### 2. Wireguard Gateway Integration
- **Objective**: Facilitate the generation of VPN profiles by users.
- **Purpose**: These profiles will allow users to access their deployed machines securely and remotely.

### 3. Administrative Control
- **Objective**: Empower educators with the ability to control all active instances.
- **Functionality**: This feature will offer educators a comprehensive overview and management tools for the instances used in their courses.

### 4. Plug & Play Challenges
- **Objective**: Allow educators to easily create and add new challenges.
- **Method**: Utilize simple Docker compose-based configurations for the rapid deployment of challenges on the site, enhancing the variety and scope of learning materials.

### 5. Leaderboard and Dynamic Challenge Secrets
- **Objective**: Introduce a competitive and interactive element to the challenges.
- **Features**:
  - Dynamic generation of challenge secrets/flags for each instance.
  - A system for students to submit these secrets/flags linked to their accounts.
  - Implementation of a leaderboard to reward points and recognize students who complete challenges promptly.

## Technology Used

- **Python**: Used for back-end development and scripting tasks.
- **Docker**: Employed for containerizing applications and challenges, ensuring a consistent and secure environment.
- **HTML, CSS, and JavaScript**: Utilized for developing the user interface and front-end functionalities of the dashboard.

---

**Note**: These future plans aim to expand the educational potential of the Machine Dashboard, making it a more versatile and engaging tool for both students and educators in the field of penetration testing.
